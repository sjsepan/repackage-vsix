# Re-package VSIX, a Bash script

## About

	Re-zips the package source and renames to VSIX. Does not validate the contents, but handles the routine tasks of zipping/renaming.
	This is not intended to compete with the npm tool vsce; it only handles this simple case of re-packing a VSIX file in a project laid out the way I have mine.

## Setup

	Make sure the script file is in a location that is on your PATH, and has Execute permission.

## Usage

	Assumes that the VSIX package source is in a folder just under the project root, and that the .VSIX file will be generated in the project root. Run the script from the project root. Give the base VSIX filename with extension as the first parameter, and the source subfolder name as the second parameter.

## Issues

The repackage-vsix.sh used to work on OVSX, but now the published pkg does not activate. I uploaded the vsce script I am using now, as a work-around.

## Contact

Stephen J Sepan
sjsepan@yahoo.com
9-19-2024
