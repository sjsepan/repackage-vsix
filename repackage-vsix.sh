#!/bin/bash
#Does not validate the contents of the package folder; only does the work of zipping/renaming

#debug info
echo "
\$# = $#
\$0 = $0
\$1 = $1
\$2 = $2
\$3 = $3
\$? = $?
"

#temp pkg name
TEMPPKG="pkg.zip"

#get name of vsix and check existing
PKGFILE=$1
if [ -e "$PKGFILE" ]; then #may exist
	ISPREVIOUSPKG="Y"
else
	ISPREVIOUSPKG="N"
fi
echo "Package File: $PKGFILE"
echo "Previous Package: $ISPREVIOUSPKG"

#get name of package root folder and validate
RESULT=$(file "$2")
if [ "$RESULT" = "$2: directory" ]; then #must exist, must be folder (of extension contents)
	PKGFOLDER=$2
else
	echo "$2 invalid: $RESULT"
	exit 1
fi 
echo "Source Folder: $PKGFOLDER"

#remove existing temp pkg zip (from failed runs)
#if exist, delete
if [ -e $TEMPPKG ]; then #may exist
	rm $TEMPPKG
fi

#zip current package source
echo "zipping..."
(cd "$PKGFOLDER" || exit; zip -r -0 "../$TEMPPKG" *)

#must exist temp pkg zip
if [ -e $TEMPPKG ]; then #must exist
	echo "$TEMPPKG created..."
else
	echo "$TEMPPKG not created"
	exit 1
fi

#if exist, delete
echo "deleting previous package..."
if [ -e "$PKGFILE" ]; then #may exist
	rm "$PKGFILE" || exit
fi

#rename new vsix
echo "renaming new package"
mv $TEMPPKG "$PKGFILE"
if [ -e "$PKGFILE" ]; then #may exist
	echo "$PKGFILE created"
else
	echo "$PKGFILE not created"
	exit 1
fi


