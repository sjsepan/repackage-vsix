#!/bin/bash
#Does not validate the contents of the package folder; only does the work of zipping/renaming

#info
echo "info:must run in 'pkg/extension' folder"

#run
cd pkg/extension
vsce package -o ../..
cd -

#done
echo "info:run completed"
